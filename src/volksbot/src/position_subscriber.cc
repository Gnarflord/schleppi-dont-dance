#include "ros/ros.h"
#include "signal.h"

#include <iostream> // std::cout, std::endl, std::cin
#include <fstream>  // std::fstream

#include "nav_msgs/Odometry.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"

#define FILE_SAVE true

std::ofstream odom_file;
std::ofstream amcl_file;

void OnAMCLReceive(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
	if (FILE_SAVE)
		// Save data to recording file
		amcl_file << msg->pose.pose.position.x << " " << msg->pose.pose.position.y << "\n";
}

void OnOdomReceive(const nav_msgs::Odometry::ConstPtr& msg)
{
	if (FILE_SAVE)
		// Save data to recording file
		odom_file << msg->pose.pose.position.x << " " << msg->pose.pose.position.y << "\n";
}


void onInterrupt(int sig)
{
	if (FILE_SAVE) {
		odom_file.close();
		amcl_file.close();
	}

	ros::shutdown();
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "pos_listener", ros::init_options::NoSigintHandler);

  ros::NodeHandle n;

	// maximum of 100 messages in queue
  ros::Subscriber odom_sub = n.subscribe("odom", 100, OnOdomReceive);
  ros::Subscriber amcl_sub = n.subscribe("amcl_pose", 100, OnAMCLReceive);

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */

	if (FILE_SAVE) {
		odom_file.open("/tmp/path_odom_data.csv");
		amcl_file.open("/tmp/path_amcl_data.csv");
	}

	// Override the default ros sigint handler.
  // This must be set after the first NodeHandle is created.
  signal(SIGINT, onInterrupt);

  ros::spin();

  return 0;
}
