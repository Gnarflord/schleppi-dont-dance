#include "ros/ros.h"
#include "signal.h"
#include "math.h"

#include <iostream> // std::cout, std::endl, std::cin
#include <fstream>  // std::fstream

#include "volksbot/vels.h"
#include "gio_path.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "nav_msgs/Odometry.h"

#define AMCL_CONTROL true
#define ODOM_CONTROL false

#define SIMULATOR false

std::ofstream amcl_file;
std::ofstream odom_file;

struct Pose2D {
	double x;
	double y;
	double theta;
};

Pose2D start_pose = {0, 0, 0};

static bool first_amcl_package = true;
static bool first_odom_package = true;

CGioController* gio = new CGioController();
ros::Publisher publisher;

double quaternion_to_yaw(geometry_msgs::Quaternion orient){
	double q0 = orient.w;
	double q1 = orient.x;
	double q2 = orient.y;
	double q3 = orient.z;

	return atan2(2* q0 * q3, 1 - (2 * q3 * q3));
}


void sendSpeed(double leftvel, double rightvel) {
  volksbot::vels velocity;
  velocity.left = leftvel;
  velocity.right = rightvel;
  publisher.publish(velocity);
}

void savePoint(double x, double y, std::ofstream& file) {
	// Save data to recording file
	file << x << " " << y << "\n";
}

void closeFiles() {
		odom_file.close();
		amcl_file.close();
}

void handleNewPose(Pose2D newPose) {

	gio->setPose(
			cos(-start_pose.theta) * (newPose.x - start_pose.x) - sin(-start_pose.theta) * (newPose.y - start_pose.y),
			sin(-start_pose.theta) * (newPose.x - start_pose.x) + cos(-start_pose.theta) * (newPose.y - start_pose.y),
			newPose.theta - start_pose.theta);

	if (AMCL_CONTROL || ODOM_CONTROL) {
		double u, w, vleft, vright;
		gio->getNextState(u, w, vleft, vright);

		sendSpeed(-vleft, -vright);
	}
}


void OnOdomReceive(const nav_msgs::Odometry::ConstPtr& msg) {
	Pose2D odom_pose;

	odom_pose.x = msg->pose.pose.position.x;
	odom_pose.y = msg->pose.pose.position.y;

	odom_pose.theta = quaternion_to_yaw(msg->pose.pose.orientation);

	// Use odometry packages for path follower if we want exclusive ODOM Control
		// If no position (either odom or amcl) was received use it as start
		// position. AMCL won't start driving on it's own, but it will overwrite
		// the start position with a more accurate guess
	if (first_odom_package && first_amcl_package) {
		first_odom_package = false;

		start_pose = odom_pose;
		handleNewPose(odom_pose);
	}

	if (ODOM_CONTROL){
		handleNewPose(odom_pose);
	}

	savePoint(odom_pose.x, odom_pose.y, odom_file);
}


void OnAMCLReceive(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg) {
	Pose2D amcl_pose;

	amcl_pose.x = msg->pose.pose.position.x;
	amcl_pose.y = msg->pose.pose.position.y;

	amcl_pose.theta = quaternion_to_yaw(msg->pose.pose.orientation);

	if (AMCL_CONTROL) {
		if (first_amcl_package) {
			first_amcl_package = false;

			start_pose = amcl_pose;
			sendSpeed(5.0, 5.0);
		}

		handleNewPose(amcl_pose);
	}

	savePoint(amcl_pose.x, amcl_pose.y, amcl_file);
}

void onInterrupt(int sig)
{
	closeFiles();
	sendSpeed(0, 0);
	ros::shutdown();
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "robot_simulator", ros::init_options::NoSigintHandler);
  ros::NodeHandle n;

  signal(SIGINT, onInterrupt);

	odom_file.open("/tmp/odom_data.csv");
	amcl_file.open("/tmp/amcl_data.csv");

//	-5.3, -4.3

  ros::Subscriber amcl_sub = n.subscribe("amcl_pose", 100, OnAMCLReceive);
  ros::Subscriber odom_sub = n.subscribe("odom", 100, OnOdomReceive);

  publisher = n.advertise<volksbot::vels>("Vel", 100);



	gio->getPathFromFile("/tmp/Beispielpfade/acht.dat");
	gio->setCurrentVelocity(15, 1); // (Speed, absolute?)

	Pose2D zero = {0, 0, 0};

	// Initial speed, such that AMCL updates are generated
	sendSpeed(5.0, 5.0);

	if (SIMULATOR) {
	  Pose2D simulator_pose;

		double theta=0, x=0, y=0;
		double dT = 0.1; // 100ms

		ros::Rate r(1/dT); // 10 hz

		double u, w, vleft, vright;

		while (gio->getNextState(u, w, vleft, vright))
		{
			simulator_pose.theta += w * dT;

			simulator_pose.x += u * dT * cos(theta);
			simulator_pose.y += u * dT * sin(theta);
			gio->setPose(simulator_pose.x, simulator_pose.y, simulator_pose.theta);

			savePoint(simulator_pose.x, simulator_pose.y, odom_file);

			ros::spinOnce();
			r.sleep();
		}
		closeFiles();
	}

	ros::spin();
  return 0;
}
