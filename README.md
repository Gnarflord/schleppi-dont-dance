# schleppi-dont-dance

Steh nicht so krum, Schleppi. Das macht die Stoßdämpfer kaputt.

# Installation
Developed under arch linux (take that nüchti) with ROS Melodic binary packages.
To install and build it yourself:
- Add this to your pacman.conf
```
[oscloud]
Server = http://repo.oscloud.info/
```
- Install ROS melodic from unofficial user repository (https://wiki.archlinux.org/index.php/Unofficial_user_repositories#oscloud)
```
sudo pacman-key --recv-keys CB222E7EBC11D682AAC8B317A4A0D73114FDE6FC
sudo pacman-key --lsign-key CB222E7EBC11D682AAC8B317A4A0D73114FDE6FC
sudo pacman -Syu
yay -S ros-melodic-desktop-full
```
- Source ROS environment (bash)
```
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
```
- Or for zsh
source /opt/ros/melodic/setup.zsh
```
echo "source /opt/ros/melodic/setup.zsh" >> ~/.zshrc
source ~/.zshrc
```
- First setup
```
sudo rosdep init
rosdep update
cd path/to/schleppi
catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python
```
- Subsequent builds can be made with
```
catkin_make
```
- Don't forget to source the project environment or you won't be able to roslaunch ze volksbot
```
source devel/setup.bash
```
or
```
source devel/setup.zsh
```

# Nice to have
```
alias cdros='cd ~/Documents/uni/SS20/schleppi-dont-dance && source devel/setup.zsh'
alias volksbotyes='cdros && roslaunch volksbot messtechnikpraktikum.launch & roslaunch volksbot localjoystick.launch & roslaunch volksbot messtechnikgmapping.launch'